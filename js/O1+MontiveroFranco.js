"use strict";
// @ts-ignore
var Observable = rxjs.Observable;
function observer() {
    var inputSelector = document.querySelector("#input");
    var mirrorSelector = document.querySelector("#mirror");
    var observable = new Observable(function (subscriber) {
        inputSelector.onkeyup = function (e) {
            e.preventDefault();
            var target = e.target;
            var value = target.value;
            subscriber.next(value.split('').reverse().join(''));
            if (value === 'complete') {
                console.log('Finaliza por que se escribio "complete"');
                target.disabled = true;
                subscriber.complete();
            }
            if (value === 'error') {
                target.disabled = true;
                subscriber.error('Finaliza por que se escribio "error"');
            }
        };
        return function () {
            inputSelector.disabled = true;
        };
    });
    var subscriber = observable
        .subscribe(function (value) { mirrorSelector.textContent = value; }, function (err) {
        if (err instanceof Error) {
            console.log(err.message);
        }
        else {
            console.log(err);
        }
    }, function () { console.log('Finalizo'); });
    setTimeout(function () {
        console.log('Se cumplieron los 30 segundos');
        subscriber.unsubscribe();
    }, 30000);
}
function main() {
    var formSelector = document.querySelector('#form');
    formSelector.onsubmit = function (e) { e.preventDefault(); };
    observer();
}
main();
