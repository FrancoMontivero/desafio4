"use strict";
// @ts-ignore
var fromEvent = rxjs.fromEvent;
// @ts-ignore
var _a = rxjs.operators, map = _a.map, finalize = _a.finalize, filter = _a.filter;
function observer() {
    var inputSelector = document.querySelector("#input");
    var mirrorSelector = document.querySelector("#mirror");
    var keyupInput = fromEvent(inputSelector, 'keyup');
    var valuesInput = keyupInput.pipe(map(function (e) {
        var target = e.target;
        var value = target.value;
        if (value === "error") {
            mirrorSelector.textContent = value;
            target.disabled = true;
            this.error('Finaliza por que se escribio "error"');
        }
        if (value === "complete") {
            mirrorSelector.textContent = value;
            console.log("Finaliza por que se escribio complete");
            target.disabled = true;
            this.complete();
        }
        return value.split('').reverse().join('');
    }), finalize(function () {
        console.log("Finalizo");
        inputSelector.disabled = true;
    }));
    var subscriber = valuesInput
        .subscribe(function (value) {
        mirrorSelector.textContent = value;
    }, function (err) {
        if (err instanceof Error) {
            console.log(err === null || err === void 0 ? void 0 : err.message);
        }
        else {
            console.log(err);
        }
    }, null);
    setTimeout(function () {
        console.log("El tiempo finalizo");
        subscriber.unsubscribe();
    }, 30000);
}
function main() {
    var formSelector = document.querySelector('#form');
    formSelector.onSubmit = function (e) { e.preventDefault(); };
    observer();
}
main();
