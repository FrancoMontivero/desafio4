interface SubscriberConsumer {
	subscribe: (
		next: (value: string) => void, 
		err: (err: Error | string) => void, 
		complete: () => void
	) => void,
	unsubscribe: () => void,
}

interface Subscriber {
	next: (value: string) => void;
	error: (err?: Error | string | null) => void;
	complete: () => void;
}

// @ts-ignore
const { Observable } = rxjs;

function observer(): void{
	const inputSelector: HTMLInputElement | null = document.querySelector("#input");
	const mirrorSelector: HTMLSpanElement | null = document.querySelector("#mirror");

	const observable: typeof Observable = new Observable((subscriber: Subscriber) => {
		inputSelector!.onkeyup! = (e: KeyboardEvent) => {
			e.preventDefault();
			const target = e!.target as HTMLInputElement;
			const value: string = target.value;
			subscriber.next(value.split('').reverse().join(''));
			if(value === 'complete'){
				console.log('Finaliza por que se escribio "complete"');
				target!.disabled = true;
				subscriber.complete();
			}
			if(value === 'error'){
				target.disabled = true;
				subscriber.error('Finaliza por que se escribio "error"');
			}

		}
		return () => {
			inputSelector!.disabled = true;
		}
	})

	const subscriber: SubscriberConsumer = observable
	.subscribe(
		(value: string): void => { mirrorSelector!.textContent = value; },
		(err: Error | string): void => { 
			if(err instanceof Error){
				console.log(err.message); 
			}else {
				console.log(err); 
			}
		},
		(): void => { console.log('Finalizo'); }
	)

	setTimeout((): void => {
		console.log('Se cumplieron los 30 segundos');
		subscriber.unsubscribe();
	}, 30000);

}

function main(): void{
	const formSelector: HTMLFormElement | null = document.querySelector('#form');
	formSelector!.onsubmit = (e: Event) => { e.preventDefault(); }
	observer();
}

main();
