// @ts-ignore
const { fromEvent } = rxjs;
// @ts-ignore
const { map, finalize, filter } = rxjs.operators;


function observer(){
	const inputSelector: HTMLInputElement | null = document.querySelector("#input");
	const mirrorSelector: HTMLSpanElement | null = document.querySelector("#mirror");
	const keyupInput = fromEvent(inputSelector, 'keyup');

	const valuesInput = keyupInput.pipe(
		map(function(this: any, e: KeyboardEvent): string{
			const target = e!.target as HTMLInputElement;
			const value = target.value;
			if(value === "error"){
				mirrorSelector!.textContent = value;
				target.disabled = true;
				this.error('Finaliza por que se escribio "error"');
			}
			if(value === "complete"){
				mirrorSelector!.textContent = value;
				console.log("Finaliza por que se escribio complete");	
				target.disabled = true;
				this.complete();
			}
			return value.split('').reverse().join('');
		}),
		finalize((): void => {
			console.log("Finalizo");
			inputSelector!.disabled = true;
		}),
	);

	const subscriber = valuesInput
	.subscribe(
		(value: string): void => {
			mirrorSelector!.textContent = value;
		},	
		(err?: string | Error | null): void => {
			if(err instanceof Error){
				console.log(err?.message);
			}else {
				console.log(err)
			}
	   	},
		null
	);

	setTimeout((): void => {
		console.log("El tiempo finalizo");
		subscriber.unsubscribe();
	}, 30000);
}

function main(): void{
	const formSelector: HTMLFormElement | null = document.querySelector('#form');
	formSelector!.onSubmit = (e: Event): void => { e.preventDefault(); };
	observer();	
}

main();
